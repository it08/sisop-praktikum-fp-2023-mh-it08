# Fungsi Drop
terbagi menjadi dua fungsi yaitu dropTable dan dropDatabase
## `int dropTable(const char *databasePath, const char *tableName)` 
fungsi ini akan menghapus file .txt yang diibaratkan sebuah table dalam database.
## `int dropDatabase(const char *databaseName)`
fungsi ini akan menghapus folder database 
### Note
dropDatabase tidak dapat berjalan, database tidak dapat terdeteksi sehingga tidak dapat terhapus
## Dokumentasi
![App Screenshot](https://i.ibb.co/x5BC5PQ/sebelum-drop-table.png)

![App Screenshot](https://i.ibb.co/74Ds2Gf/sesudah-drop-table.png)

# Fungsi Insert
## `char** split(char* str, const char* delimiter, int* count)`
Fungsi ini mengambil string str dan sebuah delimiter, lalu membagi string tersebut menjadi token-token berdasarkan delimiter. Fungsi ini mengembalikan array dari string (char**) dan memperbarui parameter count dengan jumlah token yang ditemukan.
## `bool isInteger(const char* str)`
Fungsi ini memeriksa apakah string yang diberikan mewakili bilangan bulat. Fungsi ini mengembalikan true jika string tersebut adalah bilangan bulat yang valid, dan false sebaliknya.
## `bool isDataTypeCorrect(char* value, char* dataType)`
Fungsi ini memeriksa apakah nilai yang diberikan memiliki tipe data yang sesuai dengan tipe data yang ditentukan (int atau string).
## `void removeQuotes(char* str)`
Fungsi ini menghapus tanda kutip tunggal dari string yang diberikan. Fungsi ini memodifikasi string input secara langsung.
## `int insertIntoTable(const char* tableName, const char* tableInsert)`
Ini adalah fungsi utama untuk menyisipkan data ke dalam sebuah tabel. Fungsi ini membuka file yang sesuai dengan tabel yang ditentukan, membaca kolom-kolom yang sudah ada, memvalidasi tipe data dari nilai yang akan dimasukkan, dan menambahkan data baru ke dalam file tabel.
-  Fungsi ini membuat jalur file untuk database dan tabel yang ditentukan.
-  Membuka file tabel dalam mode baca dan tulis.
-  Membaca kolom-kolom yang sudah ada beserta tipe data mereka dari file.
-  Mem-parsing nilai-nilai yang akan dimasukkan, memeriksa apakah kolom-kolom tersebut ada, dan memvalidasi tipe data mereka.
-  Menambahkan data baru ke akhir file.
-  Menutup file dan mengosongkan memori yang dialokasikan.

Fungsi ini mengembalikan nilai berbeda untuk menunjukkan hasil operasi:
- 0 jika terjadi kesalahan saat membuka file.
- 1 jika penyisipan berhasil.
- 2 jika terjadi ketidakcocokan tipe data.
- 3 jika kolom tidak ditemukan.
## Dokumentasi
![App Screenshot](https://i.ibb.co/ZhXPLbs/insert.png)

# DELETE table
fungsi ini hanya memiliki satu fungsi yaitu `int clearTableData(const char* tableName)`
Fungsi ini digunakan untuk menghapus semua data dari sebuah tabel. Berikut langkah-langkahnya:

- Membentuk path file untuk database dan tabel yang spesifik.
- Membuka file tabel untuk membaca data (mode "r").
- Jika file tidak dapat dibuka, mencetak pesan kesalahan dan mengembalikan nilai 0.
- Membaca dan menyimpan definisi data (kolom dan tipe data) dari file.
- Menutup file setelah membaca definisi data.
- Membuka kembali file tabel untuk menulis data (mode "w"), yang pada dasarnya akan mengosongkan file.
- Jika file tidak dapat dibuka, mencetak pesan kesalahan dan mengembalikan nilai 0.
- Menulis kembali definisi data ke dalam file (sehingga struktur tabel tetap utuh).
- Menutup file setelah menulis kembali definisi data.

Fungsi ini mengembalikan nilai 1 jika operasi berhasil.
## Dokumentasi
![App Screenshot](https://i.ibb.co/cDtFRMy/delete.png)

# Fungsi Select
fungsi ini hanya memiliki 1 fungsi yaitu `char* printTableData(const char* tableName)`. Fungsi ini digunakan untuk membaca data dari sebuah tabel. Langkah-langkahnya adalah sebagai berikut:

- Membentuk path file untuk database dan tabel yang spesifik.
- Membuka file tabel untuk membaca data (mode "r").
- Jika file tidak dapat dibuka, mencetak pesan kesalahan dan mengembalikan nilai 'NULL'.
- Membaca setiap baris dari file dan menyimpannya ke dalam string result setelah menemukan tanda ':'. Ini menunjukkan awal dari bagian data dalam file.
- Menutup file setelah selesai membaca.

Fungsi ini mengembalikan pointer ke string yang berisi data tabel. 

## Dokumentasi
![App Screenshot](https://i.ibb.co/ftMwLRL/select.png)

# Fungsi Create User
Fungsi ini untuk membuat user database.
```c
int createUser(const char* username, const char* password) {
    // Periksa apakah hak sudo tidak sama dengan 1
    if (sudo != 1) {
        // Cetak pesan kesalahan dan kembalikan 0 jika tidak memiliki hak akses sudo
        printf("Fungsi ini hanya dapat dijalankan dengan hak akses sudo.\n");
        return 0;
    }

    // Buka file "users.txt" dalam mode append
    FILE *file = fopen("users.txt", "a");

    // Tulis username dan password ke dalam file
    fprintf(file, "%s : %s\n", username, password);

    // Tutup file
    fclose(file);

    // Cetak pesan sukses dan kembalikan 1
    printf("Pengguna berhasil dibuat!\n");
    return 1;
}
```
- Fungsi `createUser` menerima dua parameter: `username` dan `password`, keduanya direpresentasikan sebagai string (const char*).
- Fungsi memeriksa apakah variabel `sudo` tidak sama dengan 1. Ini menunjukkan bahwa fungsi ini memerlukan tingkat hak istimewa tertentu (hak akses sudo) untuk dijalankan. Jika kondisi ini benar, fungsi mencetak pesan kesalahan dan mengembalikan 0, menandakan kegagalan.
- Jika kondisi dalam pernyataan `if` salah, fungsi melanjutkan untuk membuka file bernama "users.txt" dalam mode append ("a"). Jika file tersebut tidak ada, maka akan dibuat. Pointer file disimpan dalam variabel `file`.
- Fungsi kemudian menggunakan fungsi `fprintf` untuk menulis `username` dan `password` ke dalam file dengan format "%s : %s\n". Format ini menunjukkan bahwa informasi setiap pengguna ditulis dalam baris baru dengan `username` dan `password` dipisahkan oleh titik dua dan spasi.
- Setelah menulis informasi, file ditutup menggunakan fungsi `fclose`.
- Jika semuanya berhasil sampai pada titik ini, fungsi mencetak pesan keberhasilan yang menunjukkan bahwa pengguna telah dibuat, dan fungsi mengembalikan 1 untuk menandakan keberhasilan.

## Dokumentasi
![App Screenshot](https://i.ibb.co/RTx4gr5/Screenshot-from-2023-12-17-21-44-51.png)
![App Screenshot](https://i.ibb.co/DWFBhQz/Screenshot-from-2023-12-17-22-05-21.png)

# Fungsi Create DB
Fungsi ini untuk membuat database baru
```c
int createDatabase(const char* databaseName) {
    char* databasesPath = "./database/";
    
    // Mengalokasikan memori untuk menyimpan path lengkap database
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    
    // Menggabungkan path databasesPath dan databaseName
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    // Cetak pesan bahwa fungsi createDatabase sedang berjalan
    printf("createDatabase run\n");

    // Periksa apakah database sudah ada
    if (!access(databasePath, F_OK)) {
        // Jika sudah ada, cetak pesan dan kembalikan nilai 2
        printf("database already exists.\n");
        return 2;
    } else {
        // Jika belum ada, coba membuat direktori baru
        if (!mkdir(databasePath, 0777)) {
            // Jika berhasil, cetak pesan sukses dan kembalikan nilai 1
            printf("database created successfully!\n");
            return 1;
        } else {
            // Jika gagal membuat direktori, cetak pesan gagal dan kembalikan nilai 0
            printf("Failed to create database.\n");
            return 0;
        }
    }

    // Hindari kebocoran memori dengan melepaskan memori yang dialokasikan
    free(databasePath);
}
```
- Fungsi `createDatabase` menerima parameter `databaseName`, yang merupakan nama database yang ingin dibuat.
- Sebuah string `databasesPath` diinisialisasi dengan `./database/`. Ini adalah path relatif ke direktori tempat database akan dibuat.
- Memori dialokasikan untuk `databasePath` yang akan menyimpan path lengkap ke direktori database. Panjang alokasi memori dihitung berdasarkan panjang `databasesPath` dan `databaseName`, dan kemudian string tersebut digabungkan menggunakan `strcpy` dan `strcat`.
- Pesan "createDatabase run" dicetak ke layar untuk memberi tahu bahwa fungsi `createDatabase` sedang berjalan.
- Fungsi menggunakan fungsi `access` untuk memeriksa apakah direktori database sudah ada (`F_OK` menunjukkan bahwa path tersebut ada). Jika sudah ada, cetak pesan bahwa database sudah ada dan kembalikan nilai 2.
- Jika direktori belum ada, fungsi mencoba membuatnya menggunakan fungsi `mkdir`. Jika berhasil, cetak pesan sukses dan kembalikan nilai 1. Jika gagal, cetak pesan bahwa pembuatan database gagal dan kembalikan nilai 0.
- Terakhir, memori yang dialokasikan untuk `databasePath` dibebaskan menggunakan fungsi `free` untuk menghindari kebocoran memori.

## Dokumentasi
![App Screenshot](https://i.ibb.co/R2YxccK/Screenshot-from-2023-12-17-22-22-33.png)
![App Screenshot](https://i.ibb.co/VqDHcDp/Screenshot-from-2023-12-17-22-25-33.png)

# Fungsi use Database
untuk menggunakan database
```c
void useDatabase(const char* databaseName) {
    char* databasesPath = "./database/";
    
    // Mengalokasikan memori untuk menyimpan path lengkap database
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    
    // Menggabungkan path databasesPath dan databaseName
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    // Periksa apakah database sudah ada
    if (!access(databasePath, F_OK)) {
        // Jika sudah ada, cetak pesan bahwa database ada
        printf("database exists.\n");
    } else {
        // Jika belum ada, cetak pesan bahwa database tidak ada
        printf("database doesn't exist.\n");
    }

    // Hindari kebocoran memori dengan melepaskan memori yang dialokasikan
    free(databasePath);
}
```
- Fungsi `useDatabase` menerima parameter `databaseName`, yang merupakan nama database yang ingin digunakan.
- Sebuah string `databasesPath` diinisialisasi dengan `./database/`. Ini adalah path relatif ke direktori tempat database diharapkan berada.
- Memori dialokasikan untuk `databasePath` yang akan menyimpan path lengkap ke direktori database. Panjang alokasi memori dihitung berdasarkan panjang `databasesPath` dan `databaseName`, dan kemudian string tersebut digabungkan menggunakan `strcpy` dan `strcat`.
- Fungsi menggunakan fungsi `access` untuk memeriksa apakah direktori database sudah ada (`F_OK` menunjukkan bahwa path tersebut ada). Jika sudah ada, cetak pesan bahwa database ada.
- Jika direktori belum ada, cetak pesan bahwa database tidak ada.
- Terakhir, memori yang dialokasikan untuk `databasePath` dibebaskan menggunakan fungsi `free` untuk menghindari kebocoran memori.

# Fungsi grant Permission
untuk memberi akses pada user untuk mengakses sebuah database
```c
void grantPermissiontoaccessDatabase(const char* username, const char* databaseName) {
    // Periksa apakah pengguna memiliki hak sudo
    if (geteuid() != 0) {
        printf("This function can only be executed with sudo privileges.\n");
        return;
    }

    char* databasesPath = "./database/";

    // Mengalokasikan memori untuk menyimpan path lengkap database
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);

    // Menggabungkan path databasesPath dan databaseName
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    // Periksa apakah database sudah ada
    if (!access(databasePath, F_OK)) {
        // Jika sudah ada, buka file "user.txt" dalam mode append
        FILE *file = fopen("user.txt", "a");

        // Menulis informasi permission ke dalam file
        fprintf(file, "%s:%s\n", username, databaseName);

        // Tutup file
        fclose(file);

        // Cetak pesan bahwa izin diberikan dengan sukses
        printf("Permission granted successfully!\n");
    } else {
        // Jika database tidak ada, cetak pesan bahwa database tidak ada
        printf("database doesn't exist.\n");
    }

    // Hindari kebocoran memori dengan melepaskan memori yang dialokasikan
    free(databasePath);
}
```
- Fungsi `grantPermissiontoaccessDatabase` menerima dua parameter, yaitu `username` dan `databaseName`, yang merupakan informasi tentang izin yang akan diberikan.
- Fungsi ini pertama-tama memeriksa apakah pengguna yang menjalankan fungsi memiliki hak sudo dengan menggunakan `geteuid()`. Jika tidak, fungsi mencetak pesan kesalahan dan mengembalikan `void`, menunjukkan bahwa fungsi ini hanya dapat dijalankan dengan hak sudo.
- Sebuah string `databasesPath` diinisialisasi dengan `./database/`. Ini adalah path relatif ke direktori tempat database diharapkan berada.
- Memori dialokasikan untuk `databasePath` yang akan menyimpan path lengkap ke direktori database. Panjang alokasi memori dihitung berdasarkan panjang `databasesPath` dan `databaseName`, dan kemudian string tersebut digabungkan menggunakan `strcpy` dan `strcat`.
- Fungsi menggunakan fungsi `access` untuk memeriksa apakah direktori database sudah ada (`F_OK` menunjukkan bahwa path tersebut ada).
- Jika direktori sudah ada, fungsi membuka file "user.txt" dalam mode append, menulis informasi permission ke dalam file, menutup file, dan mencetak pesan bahwa izin diberikan dengan sukses.
- Jika direktori tidak ada, fungsi mencetak pesan bahwa database tidak ada.
- Terakhir, memori yang dialokasikan untuk `databasePath` dibebaskan menggunakan fungsi `free` untuk menghindari kebocoran memori.

# Fungsi CREATE TABLE
untuk membuat table pada database
```c
void createTable(const char* databaseName, const char* tableName, char* columns[]) {
    char* databasesPath = "./database/";

    // Mengalokasikan memori untuk menyimpan path lengkap database
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);

    // Menggabungkan path databasesPath dan databaseName
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    // Periksa apakah database sudah ada
    if (!access(databasePath, F_OK)) {
        // Mengalokasikan memori untuk menyimpan path lengkap tabel
        char* tablePath = malloc(strlen(databasePath) + strlen(tableName) + 2);
        
        // Menggabungkan path databasePath, "/", dan tableName
        strcpy(tablePath, databasePath);
        strcat(tablePath, "/");
        strcat(tablePath, tableName);

        // Buka file tabel untuk ditulis (overwrite mode)
        FILE *file = fopen(tablePath, "w");

        // Menulis kolom-kolom ke dalam file
        for (int i = 0; columns[i] != NULL; i++) {
            fprintf(file, "%s\n", columns[i]);
        }

        // Tutup file
        fclose(file);

        // Cetak pesan bahwa tabel berhasil dibuat
        printf("Table created successfully!\n");
    } else {
        // Jika database tidak ada, cetak pesan bahwa database tidak ada
        printf("Database doesn't exist.\n");
    }

    // Hindari kebocoran memori dengan melepaskan memori yang dialokasikan
    free(databasePath);
}
```
Fungsi `createTable` menerima tiga parameter, yaitu `databaseName`, `tableName`, dan `columns`. `columns` adalah array yang berisi definisi kolom-kolom tabel.

Fungsi ini melakukan langkah-langkah berikut:

1. Membuat path lengkap ke direktori database dengan mengalokasikan memori untuk `databasePath`, menggabungkan `databasesPath` dan `databaseName` menggunakan `strcpy` dan `strcat`.
2. Memeriksa apakah database sudah ada dengan menggunakan `access` dan mode `F_OK`. Jika database tidak ada, fungsi mencetak pesan kesalahan dan mengembalikan `void`.
3. Jika database sudah ada, fungsi mengalokasikan memori untuk `tablePath` yang akan menyimpan path lengkap ke file tabel. Path ini dihasilkan dengan menggabungkan `databasePath`, "/", dan `tableName`.
4. Membuka file tabel dengan mode "w" (write/overwrite) menggunakan `fopen`. Jika file sudah ada, isinya akan dihapus.
5. Menggunakan loop `for` untuk menulis setiap elemen array `columns` ke dalam file menggunakan `fprintf`.
6. Setelah selesai menulis, file ditutup dengan `fclose`.
7. Fungsi mencetak pesan bahwa tabel berhasil dibuat.
8. Jika database tidak ada, fungsi mencetak pesan bahwa database tidak ada.
9. Terakhir, memori yang dialokasikan untuk `databasePath` dan `tablePath` dibebaskan menggunakan `free` untuk menghindari kebocoran memori.

# Fungsi Log
untuk membuat log setiap command yang dilakukan
```c
void logCommand(const char* username, const char* command) {
    // Buka file log.txt dalam mode append
    FILE *file = fopen("log.txt", "a");
    
    // Periksa apakah file berhasil dibuka
    if (file == NULL) {
        // Jika gagal membuka file, cetak pesan kesalahan dan kembalikan void
        printf("Failed to open log file.\n");
        return;
    }

    // Mendapatkan waktu saat ini
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // Menulis informasi log ke dalam file
    fprintf(file, "%04d-%02d-%02d %02d:%02d:%02d:%s:%s\n", 
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, 
            tm.tm_hour, tm.tm_min, tm.tm_sec, 
            username, command);

    // Tutup file
    fclose(file);
}
```
- Fungsi `logCommand` menerima dua parameter, yaitu `username` dan `command`, yang mewakili informasi yang akan dicatat dalam file log.
- Fungsi membuka file "log.txt" dalam mode append ("a") menggunakan `fopen`.
- Fungsi melakukan pemeriksaan apakah file berhasil dibuka. Jika gagal, fungsi mencetak pesan kesalahan dan kembali tanpa melakukan lebih lanjut.
- Fungsi menggunakan fungsi `time` untuk mendapatkan waktu saat ini dalam detik sejak Epoch (1 Januari 1970). Selanjutnya, menggunakan `localtime` untuk mengonversi waktu tersebut ke dalam struktur `tm` yang berisi informasi waktu terperinci.
- Fungsi menggunakan `fprintf` untuk menulis informasi log ke dalam file. Format waktu dan informasi `username` serta `command` ditulis ke dalam file log.
- Setelah menulis informasi, file ditutup menggunakan `fclose`.

## Dokumentasi
![App Screenshot](https://i.ibb.co/VWWFbHs/Screenshot-from-2023-12-17-22-46-04.png)

# Fungsi int main()
```c
int main() {
    int serverSocket, clientSocket;
    struct sockaddr_in serverAddr, clientAddr;
    socklen_t addrLen = sizeof(struct sockaddr_in);
    char buffer[MAX_BUFFER_SIZE];

    // Create socket
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Configure server address
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = INADDR_ANY;

    // Bind the socket
    if (bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        perror("Bind failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(serverSocket, 5) == -1) {
        perror("Listen failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d...\n", PORT);

    // Accept client connection
    if ((clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddr, &addrLen)) == -1) {
        perror("Accept failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    printf("Client connected from %s:%d\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));


    // Receive and handle commands
    while (1) {
        memset(buffer, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, buffer, sizeof(buffer), 0) <= 0) {
            perror("Receive failed");
            close(serverSocket);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        handleCommand(buffer, clientSocket);

        // Check for EXIT command
        if (strncmp(buffer, "EXIT", 4) == 0) {
            printf("Server shutting down...\n");
            break;
        }
    }

    close(serverSocket);
    close(clientSocket);

    return 0;
}
```

Merupakan fungsi utama dalam program ini. Digunakan untuk menjalankan database.c

# Fungsi handleCommand()
```c
void handleCommand(char* command, int clientSocket) {
    if (strncmp(command, "LOGIN", 5) == 0){
        char username[50];
        char password[50];
        if (sscanf(command, "LOGIN %s %s", username, password) == 2) {
            printf("Received username: %s\n", username);
            printf("Received password: %s\n", password);
            if (strcmp(username, "root")==0 && strcmp(password, "toor")==0){
                sudo = 1;
                printf("%d\n",sudo);
            }
            strcpy(usernameGlobal, username);
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else {
            send(clientSocket, "Invalid login format", sizeof("Invalid login format"), 0);
        }
    } else if (strncmp(command, "CREATE USER", 11) == 0){
        char nama_user[50];
        char password_user[50];
        sscanf(command, "CREATE USER %s IDENTIFIED BY %s;", nama_user, password_user);
        printf("nama_user: %s\n", nama_user);
        printf("password_user: %s\n", password_user);
        if (createUser(nama_user, password_user) == 1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (createUser(nama_user, password_user) == 0){
            send(clientSocket, "This function can only be executed with sudo privileges.", sizeof("This function can only be executed with sudo privileges."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else if (strncmp(command, "CREATE TABLE", 12) == 0) {
        char tableName[50];
        char columnsStr[500];
        sscanf(command, "CREATE TABLE %s (%[^);]", tableName, columnsStr);
        printf("Table Name: %s\n", tableName);
        printf("Columns: %s\n", columnsStr);

        // Create a copy of columnsStr that can be modified
        char columnsCopy[500];
        strcpy(columnsCopy, columnsStr);

        // Ubah ke array

        printf("Setelah inisiasi token\n");

        // Initialize an array to store the tokens
        char *columns[100];  // Assuming a maximum of 100 tokens, adjust as needed
        int count = 0;

        printf("Setelah inisiasi columns\n");

        char *token = strtok(columnsCopy, ",");
        // // Loop through the tokens and store them in the array
        while (token != NULL) {
            // Remove leading and trailing whitespaces from the token
            char *trimmedToken = trimSpaces(token);

            // Allocate memory for the token and copy it to the array
            columns[count] = malloc(strlen(trimmedToken) + 1);
            strcpy(columns[count], trimmedToken);

            // Move to the next token
            token = strtok(NULL, ",");
            ++count;
        }

        for (int i = 0; i < count; ++i) {
            printf("columns[%d] = %s\n", i, columns[i]);
        }

        printf("Setelah while\n");

        if (strlen(databaseNameGlobal)<=0){
            printf("Database belum dipilih");
            send(clientSocket, "Database not yet been chosen", sizeof("Database not yet been chosen"), 0);
        } else {
            if (createTable(databaseNameGlobal, tableName, columns, count) == 1) {
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else if (createTable(databaseNameGlobal, tableName, columns, count) == 0) {
                send(clientSocket, "Database doesn't exist.", sizeof("Database doesn't exist."), 0);
            } else {
                send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
            }
        }
    } else if (strncmp(command, "CREATE DATABASE", 15) == 0){
        char database[50];
        sscanf(command, "CREATE DATABASE %s", database);
        printf("database: %s\n", database);
        if (createDatabase(database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (createDatabase(database)==2){
            send(clientSocket, "database already exists.", sizeof("database already exists."), 0);
        } else if (createDatabase(database)==0){
            send(clientSocket, "Failed to create database.", sizeof("Failed to create database."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else if (strncmp(command, "USE", 3) == 0){
        char database[50];
        sscanf(command, "USE %s", database);
        printf("database: %s\n", database);
        if (useDatabase(database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            strcpy(databaseNameGlobal, database);
        } else if (useDatabase(database)==0){
            send(clientSocket, "database doesn't exist.", sizeof("database doesn't exist."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
        printf("%s\n", databasePathGlobal);
    } else if (strncmp(command, "GRANT PERMISSION", 16) == 0){
        char database[50];
        char nama_user[50];
        sscanf(command, "GRANT PERMISSION %s INTO %s", database, nama_user);
        printf("nama_user: %s\n", nama_user);
        printf("database: %s\n", database);
        if (grantPermissiontoaccessDatabase(nama_user, database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (grantPermissiontoaccessDatabase(nama_user, database)==2){
            send(clientSocket, "database doesn't exist.", sizeof("database doesn't exist."), 0);
        } else if (grantPermissiontoaccessDatabase(nama_user, database)==0){
            send(clientSocket, "This function can only be executed with sudo privileges.", sizeof("This function can only be executed with sudo privileges."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "DROP", 4) == 0){
        if (strncmp(command, "DROP DATABASE", 13) == 0){
            char database[50];
            sscanf(command, "DROP DATABASE %s", database);
            printf("database: %s\n", database);
            if (dropDatabase(database)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "Database Not Found", sizeof("Database Not Found"), 0);
            }
        } else if (strncmp(command, "DROP TABLE", 10) == 0){
            char table[50];
            sscanf(command, "DROP TABLE %s", table);
            printf("Table: %s\n", table);
            if (dropTable(databasePathGlobal, table)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "Table not found", sizeof("Table not found"), 0);
            }
        } else if (strncmp(command, "DROP COLUMN", 11) == 0){
            char kolom[50];
            char table[50];
            sscanf(command, "DROP COLUMN %s FROM %s", kolom, table);
            printf("Kolom: %s\n", kolom);
            printf("Table: %s\n", table);
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        }
    } else if (strncmp(command, "UPDATE", 6) == 0){
        char tableName[50];
        char columnName[50];
        char value[50];
        sscanf(command, "UPDATE %s SET %s=%[^;];", tableName, columnName, value);
        printf("Table Name: %s\n", tableName);
        printf("Column Name: %s\n", columnName);
        printf("Value: %s\n", value);
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "DELETE FROM", 11) == 0){
        char table[50];
        sscanf(command, "DELETE FROM %s", table);
        printf("Table: %s\n", table);
        if (strlen(databasePathGlobal)<=0){
            send(clientSocket, "Database not been chosen", sizeof("Database not been chosen"), 0);
        } else {
            if (clearTableData(table)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
            }
        }
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "INSERT INTO", 11) == 0){
        char table[50];
        char columns[500];
        sscanf(command, "INSERT INTO %s (%[^);]", table, columns);
        printf("Table: %s\n", table);
        printf("Isi: %s\n", columns);
        if (insertIntoTable(table, columns) == 1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (insertIntoTable(table, columns) == 2){
            send(clientSocket, "Error data type", sizeof("Error data type"), 0);
        } else if (insertIntoTable(table, columns) == 3){
            send(clientSocket, "Columns not found", sizeof("Columns not found"), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }   
    } else if (strncmp(command, "SELECT FROM", 11)==0){
        char table[50];
        sscanf(command, "SELECT FROM %s", table);
        char* tableData = printTableData(table);
        printf("%s", tableData);
        if (strlen(tableData) > 0){
            send(clientSocket, tableData, strlen(tableData), 0);
            free(tableData);
        } else if (strlen(tableData) <= 0) {
            send(clientSocket, "There is a no data", sizeof("There is no data"), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else {
        send(clientSocket, "Invalid command", sizeof("Invalid command"), 0);
    }
    logCommand(usernameGlobal, command);
}
```

Digunakan untuk mengelola command yang diterima dari client.c


# Client.c
```c
// client.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 3000
#define MAX_BUFFER_SIZE 1024
#define MAX_STRING_SIZE 50

void processCommandsFromFile(int clientSocket, const char *filename);

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s -u <username> -p <password>\n", programName);
    fprintf(stderr, "Usage: %s -u <username> -p <password> [-d <database>] [-f <filename>]\n", programName);
    exit(EXIT_FAILURE);
}

void handleSocketError(int clientSocket) {
    perror("Socket operation failed");
    close(clientSocket);
    exit(EXIT_FAILURE);
}

// Fungsi untuk autentikasi
int authenticate(const char *username, const char *password, const char *credentialFile) {
    printf("Jalan auten\n");

    FILE *credFile = fopen(credentialFile, "r");
    if (credFile == NULL) {
        perror("Error membuka file kredensial");
        return 0;  // Autentikasi gagal
    }

    char storedUsername[50];
    char storedPassword[50];
    int authenticated = 0;

    // Membaca setiap baris dalam file kredensial
    while (fscanf(credFile, "%s : %s", storedUsername, storedPassword) == 2) {
        // Memeriksa apakah username dan password cocok
        printf("storedUsername: %s\n", storedUsername);
        printf("storedPassword: %s\n", storedPassword);
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            authenticated = 1;  // Autentikasi berhasil
            break;
        }
    }

    fclose(credFile);

    return authenticated;
}

int main(int argc, char *argv[]) {
    int clientSocket;
    struct sockaddr_in serverAddr;
    char command[MAX_BUFFER_SIZE];
    char response[MAX_BUFFER_SIZE];

    char username[MAX_STRING_SIZE] = "";
    char password[MAX_STRING_SIZE] = "";
    char database[MAX_STRING_SIZE] = "";

    const char *filename = NULL;

    if (geteuid() == 0) {
        printf("Running as root, performing actions without login.\n");
        strcpy(username, "root");
        strcpy(password, "toor");

        if (argc == 5 && strcmp(argv[1], "-d") == 0 && strcmp(argv[3], "-f") == 0) {
            strncpy(database, argv[2], MAX_STRING_SIZE - 1);
            filename = argv[4];
        } else if (argc != 1) {
            printUsage(argv[0]);
        }
    } else {
        if (argc < 4 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            printUsage(argv[0]);
            exit(EXIT_FAILURE);
        }

        if (authenticate(argv[2], argv[4], "users.txt")){
            strncpy(username, argv[2], MAX_STRING_SIZE - 1);
            strncpy(password, argv[4], MAX_STRING_SIZE - 1);
        } else {
            printf("Wrong Username and Password!");
            exit(EXIT_FAILURE);
        }

        if (argc == 9 && strcmp(argv[5], "-d") == 0 && strcmp(argv[7], "-f") == 0) {
            strncpy(database, argv[6], MAX_STRING_SIZE - 1);
            filename = argv[8];
        } else if (argc != 5) {
            printUsage(argv[0]);
        }
    }

    // Create socket
    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        handleSocketError(clientSocket);
    }

    // Configure server address
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    // Connect to the server
    if (connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        handleSocketError(clientSocket);
    }

    printf("Connected to the server\n");

    //LOGIN

    snprintf(command, sizeof(command), "LOGIN %s %s", username, password);
    if (send(clientSocket, command, strlen(command), 0) == -1) {
        handleSocketError(clientSocket);
    }

    memset(response, 0, MAX_BUFFER_SIZE);
    if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
        handleSocketError(clientSocket);
    }

    printf("%s\n", response);

    //For database

    if (strlen(database) > 0) {
        snprintf(command, sizeof(command), "USE %s", database);
        if (send(clientSocket, command, strlen(command), 0) == -1) {
            handleSocketError(clientSocket);
        }
        memset(response, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
            handleSocketError(clientSocket);
        }

        printf("%s\n", response);
    }

    // Receive and handle commands
    if (filename != NULL) {
        processCommandsFromFile(clientSocket, filename);
        while (1) {
            printf("Enter command: ");
            fgets(command, MAX_BUFFER_SIZE, stdin);

            // Send the command to the server
            if (send(clientSocket, command, strlen(command), 0) == -1) {
                handleSocketError(clientSocket);
            }

            // Check for EXIT command
            if (strncmp(command, "EXIT", 4) == 0) {
                break;
            }

            // Receive and print the server's response
            memset(response, 0, MAX_BUFFER_SIZE);
            if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
                handleSocketError(clientSocket);
            }

            printf("%s\n", response);
        }
    } else {
        while (1) {
            printf("Enter command: ");
            fgets(command, MAX_BUFFER_SIZE, stdin);

            // Send the command to the server
            if (send(clientSocket, command, strlen(command), 0) == -1) {
                handleSocketError(clientSocket);
            }

            // Check for EXIT command
            if (strncmp(command, "EXIT", 4) == 0) {
                break;
            }

            // Receive and print the server's response
            memset(response, 0, MAX_BUFFER_SIZE);
            if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
                handleSocketError(clientSocket);
            }

            printf("%s\n", response);
        }
    }

    close(clientSocket);

    return 0;
}

void processCommandsFromFile(int clientSocket, const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    char command[MAX_BUFFER_SIZE];
    char response[MAX_BUFFER_SIZE];

    // Read and send commands from the file
    while (fgets(command, sizeof(command), file) != NULL) {
        // Remove newline characters
        command[strcspn(command, "\r\n")] = '\0';

        // Send the command to the server
        if (send(clientSocket, command, strlen(command), 0) == -1) {
            perror("Send failed");
            fclose(file);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        // Check for EXIT command
        if (strncmp(command, "EXIT", 4) == 0) {
            break;
        }

        // Receive and print the server's response
        memset(response, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
            perror("Receive failed");
            fclose(file);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        printf("%s\n", response);
    }

    fclose(file);
}

```

Digunakan untuk mengirimkan command dari client ke database. Memiliki fitur untuk menerima file backup command dan autentikasi.

# client_dump.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 1000
#define MAX_STRING_SIZE 50

int extractUsername(const char *input, char *username) {
    const char *start = strchr(input, ':');
    if (start != NULL) {
        start++;  // Move to the character after the first ':'
        const char *end = strchr(start, ':');
        if (end != NULL) {
            // Calculate the length of the substring between the colons
            size_t length = end - start;

            // Ensure the length is within bounds
            if (length < MAX_STRING_SIZE - 1) {
                strncpy(username, start, length);
                username[length] = '\0';  // Null-terminate the string
                return 0;  // Success
            } else {
                return 1;  // Error: username is too long
            }
        }
    }

    return 1;  // Error: colons not found in the input
}


void process_log_line(char *line, const char *targetUsername) {
    // Find the position of the fourth colon (":") in the line
    char *pos = strchr(line, ':');
    char logUsername[MAX_STRING_SIZE];

    if (pos != NULL) {
        pos = strchr(pos + 1, ':');
        // printf("IF 1 = %s", pos);
        if (pos != NULL) {
            pos = strchr(pos + 1, ':');
            // printf("IF 2 = %s", pos);

            extractUsername(pos, logUsername);

            // printf("logUserName = %s\n", logUsername);

            if (strcmp(logUsername, targetUsername) == 0){
                if (pos != NULL) {
                    pos = strchr(pos + 1, ':');
                    // printf("IF 3 = %s", pos);
                    if (pos != NULL) {
                        // Extract the username from the log line

                        // printf("IF 4 = %s", pos);

                        // Trim leading whitespaces from the SQL query
                        pos += 1;
                        while (isspace(*pos)) {
                            pos++;
                        }

                        printf("%s", pos);
                    }
                }
            }
        }
    }
}

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s -u <username> -p <password>\n", programName);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printUsage(argv[0]);
    }

    char username[MAX_STRING_SIZE] = "";
    char password[MAX_STRING_SIZE] = "";

    strncpy(username, argv[2], MAX_STRING_SIZE - 1);
    strncpy(password, argv[4], MAX_STRING_SIZE - 1);

    FILE *file = fopen("log.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    printf("Username: %s\n", username);
    printf("Password: %s\n", password);

    char line[MAX_LINE_LENGTH];

    printf("\n");

    // Read and process each line of the log file
    while (fgets(line, sizeof(line), file) != NULL) {
        process_log_line(line, username);
    }

    // Close the file
    fclose(file);

    return 0;
}

```

Digunakan untuk mengbackup seluruh command yang dimasukkan oleh user. Dengan mengambil data dari log.txt kemudian hanya menyambil bagian commandnya saja, kemudian mengouputkan seluruh command tersebut. Dapat menggunakan fitur redirection untuk menyimpan output di suatu file.

# 
