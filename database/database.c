// server.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <time.h>

#include <ctype.h>

#include <dirent.h>

#define PORT 3000
#define MAX_BUFFER_SIZE 1024

int sudo = 0;
char databasePathGlobal[1024] = "";
char databaseNameGlobal[100] = "";
char usernameGlobal[100]="";

void handleCommand(char* command, int clientSocket);

// ----------------------------------------------------------------------------
// Bagian Jojo
// ----------------------------------------------------------------------------

int createUser(const char* username, const char* password) {
    if (sudo != 1) {
        printf("This function can only be executed with sudo privileges.\n");
        return 0;
    }
    FILE *file = fopen("users.txt", "a");
    fprintf(file, "%s : %s\n", username, password);
    fclose(file);
    printf("User created successfully!\n");
    return 1;
}

int createDatabase(const char* databaseName) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    printf("createDatabase run\n");

    if (!access(databasePath, F_OK)) {
        printf("database already exists.\n");
        return 2;
    } else {
        if (!mkdir(databasePath, 0777)) {

            printf("database created successfully!3\n");
            return 1;
        } else {
            printf("Failed to create database.\n");
            return 0;
        }
    }

    free(databasePath);
}

int useDatabase(const char* databaseName) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    if (!access(databasePath, F_OK)) {
        printf("database exists.\n");
        strcpy(databasePathGlobal, databasePath);
        return 1;
    } else {
        printf("database doesn't exist.\n");
        return 0;
    }

    free(databasePath);
}

int grantPermissiontoaccessDatabase(const char* username, const char* databaseName) {
    if (sudo != 1) {
        printf("This function can only be executed with sudo privileges.\n");
        return 0;
    }
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    if (!access(databasePath, F_OK)) {
        FILE *file = fopen("userdb.txt", "a");
        fprintf(file, "%s:%s\n", username, databaseName);
        fclose(file);
        printf("Permission granted successfully!\n");
        return 1;
    } else {
        printf("database doesn't exist.\n");
        return 2;
    }

    free(databasePath);
}

int createTable(const char* databaseName, const char* tableName, char* columns[], int count) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseName) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseName);

    printf("Run createTable\n");

    if (!access(databasePath, F_OK)) {
        printf("Run first If\n");

        char* tablePath = malloc(strlen(databasePath) + strlen(tableName) + 5); // +5 for "/.txt" and null terminator
        strcpy(tablePath, databasePath);
        strcat(tablePath, "/");
        strcat(tablePath, tableName);
        strcat(tablePath, ".txt"); // Add the .txt extension

        printf("After the strcat\n");
        FILE *file = fopen(tablePath, "wt"); // Open the file in text mode
        printf("After open file\n");

        // for (int i = 0; i < 3; ++i) {
        //     printf("columns[%d] = %s\n", i, columns[i]);
        // }

        for (int i = 0; i<count; i++) {
            fprintf(file, "%s\n", columns[i]);
            printf("Write to the file: %s\n", columns[i]);
        }
        printf("After writing all of it\n");
        fclose(file);
        printf("Table created successfully!\n");

        // Free databasePath and tablePath after using them
        free(databasePath);
        free(tablePath);

        return 1;
    } else {
        printf("Database doesn't exist.\n");

        // Free databasePath in case of failure
        free(databasePath);

        return 0;
    }
}

char *trimSpaces(char *str) {
    // Trim leading spaces
    while (isspace((unsigned char)*str)) {
        str++;
    }

    // Trim trailing spaces
    char *end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end)) {
        end--;
    }

    // Null-terminate the trimmed string
    *(end + 1) = '\0';

    return str;
}

void logCommand(const char* username, const char* command) {
    FILE *file = fopen("log.txt", "a");
    if (file == NULL) {
        printf("Failed to open log file.\n");
        return;
    }

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    // Ensure there are no leading or trailing spaces in the command
    char trimmedCommand[MAX_BUFFER_SIZE];
    strcpy(trimmedCommand, trimSpaces(command));

    fprintf(file, "%04d-%02d-%02d %02d:%02d:%02d:%s:%s\n", 
            tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, 
            tm.tm_hour, tm.tm_min, tm.tm_sec, 
            username, trimmedCommand);

    fclose(file);
}

//-------------------------------------------------------------------------------
// Bagian Rea
//-------------------------------------------------------------------------------

int dropTable(const char *databasePath, const char *tableName) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(databasePath);

    if (dir == NULL) {
        perror("Error membuka direktori");
        return 0;
    }

    // Iterasi semua file/folder dalam direktori
    while ((entry = readdir(dir)) != NULL) {
        int flag = 0;

        // Skip "." dan ".." untuk menghindari penghapusan keseluruhan direktori
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Membentuk path lengkap untuk setiap entri dalam direktori
        char entryPath[256];
        sprintf(entryPath, "%s/%s", databasePath, entry->d_name);

        // Menggunakan stat untuk mendapatkan informasi tentang entri
        struct stat entryStat;
        if (stat(entryPath, &entryStat) == 0) {
            // Memeriksa apakah itu adalah direktori
            if (S_ISDIR(entryStat.st_mode)) {
                // Jika ya, panggil fungsi dropTable secara rekursif di dalam direktori ini
                dropTable(entryPath, tableName);
            } else if (S_ISREG(entryStat.st_mode)) {
                // Jika itu adalah file regular dan nama file cocok (tanpa memperhatikan ekstensi)
                char *dot = strrchr(entry->d_name, '.');
                if (dot && strcmp(dot, ".txt") == 0) {
                    *dot = '\0'; // Menghilangkan ekstensi
                }

                if (strcmp(entry->d_name, tableName) == 0) {
                    // Jika iya, hapus file
                    if (remove(entryPath) == 0) {
                        printf("Tabel '%s' berhasil dihapus.\n", tableName);
                        flag = 1;
                    } else {
                        printf("Gagal menghapus tabel '%s'. Tabel mungkin tidak ditemukan.\n", tableName);
                    }
                    closedir(dir);
                    return flag;
                }
            }
        }
    }

    closedir(dir);
}

int dropDatabase(const char *databaseName) {
    DIR *dir;
    struct dirent *entry;
    int flag = 0;

    dir = opendir(databaseName);

    if (dir == NULL) {
        perror("Error membuka direktori");
        return flag;
    }

    // Iterasi semua file/folder dalam direktori
    while ((entry = readdir(dir)) != NULL) {
        // Skip "." dan ".." untuk menghindari penghapusan keseluruhan direktori
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        // Membentuk path lengkap untuk setiap entri dalam direktori
        char entryPath[256];
        sprintf(entryPath, "%s/%s", databaseName, entry->d_name);

        // Menggunakan stat untuk mendapatkan informasi tentang entri
        struct stat entryStat;
        if (stat(entryPath, &entryStat) == 0) {
            // Jika itu adalah direktori, panggil fungsi dropDatabase secara rekursif
            if (S_ISDIR(entryStat.st_mode)) {
                dropDatabase(entryPath);
            } else if (S_ISREG(entryStat.st_mode)) {
                // Jika itu adalah file, panggil fungsi dropTable
                dropTable(databaseName, entry->d_name);
            }
        }
    }

    closedir(dir);

    // Setelah menghapus seluruh isi folder, hapus folder itu sendiri
    if (rmdir(databaseName) == 0) {
        printf("Database '%s' berhasil dihapus.\n", databaseName);
        flag = 1;
    } else {
        printf("Gagal menghapus database '%s'. Database mungkin tidak ditemukan atau tidak kosong.\n", databaseName);
    }

    return flag;
}


// -------------------------------------
// Bagian Insert
// -------------------------------------

#define MAX_COLUMNS 100
#define MAX_COLUMN_NAME 50
#define MAX_DATA_TYPE 10
#define MAX_LINE_LENGTH 1024

typedef struct {
    char columnName[MAX_COLUMN_NAME];
    char dataType[MAX_DATA_TYPE];
} Column;

char** split(char* str, const char* delimiter, int* count) {
    char** result = malloc(MAX_COLUMNS * sizeof(char*));
    char* token;
    *count = 0;
    token = strtok(str, delimiter);
    while (token != NULL) {
        result[*count] = token;
        (*count)++;
        token = strtok(NULL, delimiter);
    }
    return result;
}

bool isInteger(const char* str) {
    if (*str == '-' || *str == '+') str++; 
    if (!*str) return false; 

    while (*str) {
        if (!isdigit(*str)) return false;
        str++;
    }
    return true;
}

bool isDataTypeCorrect(char* value, char* dataType) {
    if (strcmp(dataType, "int") == 0) {
        return isInteger(value);
    } else if (strcmp(dataType, "string") == 0) {
        // For string data type, ensure the value is not an integer
        return !isInteger(value);
    }
    return true;
}


// Function to remove quotes from a string
void removeQuotes(char* str) {
    char* p = str;
    while (*str != '\0') {
        if (*str != '\'') {
            *p++ = *str;
        }
        str++;
    }
    *p = '\0';
}

int insertIntoTable(const char* tableName, const char* tableInsert) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseNameGlobal) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseNameGlobal);

    char* tablePath = malloc(strlen(databasePath) + strlen(tableName) + 5); // +5 for "/.txt" and null terminator
    strcpy(tablePath, databasePath);
    strcat(tablePath, "/");
    strcat(tablePath, tableName);
    strcat(tablePath, ".txt"); // Add the .txt extension

    FILE* file = fopen(tablePath, "r+");
    if (file == NULL) {
        perror("Error opening file");
        return 0;
    }

    char line[MAX_LINE_LENGTH];
    Column columns[MAX_COLUMNS];
    int columnCount = 0;

    while (fgets(line, MAX_LINE_LENGTH, file) && line[0] != '\n') {
        sscanf(line, "%s %s", columns[columnCount].columnName, columns[columnCount].dataType);
        columnCount++;
    }

    char* insertString = strdup(tableInsert);
    int valuesCount;
    char** values = split(insertString, ", ", &valuesCount);

    for (int i = 0; i < valuesCount; i += 2) {
        removeQuotes(values[i]); // Remove quotes from column name
        bool columnFound = false;
        for (int j = 0; j < columnCount; j++) {
            if (strcmp(values[i], columns[j].columnName) == 0) {
                columnFound = true;
                if (!isDataTypeCorrect(values[i + 1], columns[j].dataType)) {
                    fprintf(stderr, "Data type mismatch for column %s\n", columns[j].columnName);
                    return 2;
                }
                break;
            }
        }
        if (!columnFound) {
            fprintf(stderr, "Column %s not found\n", values[i]);
            return 3;
        }
    }

    fseek(file, 0, SEEK_END);
    for (int i = 0; i < valuesCount; i += 2) {
        fprintf(file, "%s:%s", values[i], values[i + 1]);
        if (i < valuesCount - 2) {
            fprintf(file, ", ");
        }
    }
    fprintf(file, "\n");

    fclose(file);
    free(values);
    free(insertString);

    return 1;
}



// -------------------------------
// Bagian Delete table
// -------------------------------

#define MAX_LINE_LENGTH 1024

int clearTableData(const char* tableName) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseNameGlobal) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseNameGlobal);

    char* tablePath = malloc(strlen(databasePath) + strlen(tableName) + 5); // +5 for "/.txt" and null terminator
    strcpy(tablePath, databasePath);
    strcat(tablePath, "/");
    strcat(tablePath, tableName);
    strcat(tablePath, ".txt"); // Add the .txt extension

    // Open the file for reading
    FILE* file = fopen(tablePath, "r");
    if (file == NULL) {
        perror("Error opening file for reading");
        return 0;
    }

    char definitions[MAX_LINE_LENGTH * 3] = ""; // Increase buffer size if needed
    char line[MAX_LINE_LENGTH];

    // Read and store the data definitions
    while (fgets(line, MAX_LINE_LENGTH, file)) {
        if (strstr(line, ":") != NULL) { // Check if the line contains data
            break;
        }
        strcat(definitions, line);
    }

    fclose(file);

    // Open the file for writing (this clears the file)
    file = fopen(tablePath, "w");
    if (file == NULL) {
        perror("Error opening file for writing");
        return 0;
    }

    // Write the data definitions back to the file
    fputs(definitions, file);

    fclose(file);

    return 1;
}


// --------------------
// Buat SELECT
// --------------------

#define MAX_LINE_LENGTH 1024

char* printTableData(const char* tableName) {
    char* databasesPath = "./database/";
    char* databasePath = malloc(strlen(databasesPath) + strlen(databaseNameGlobal) + 1);
    strcpy(databasePath, databasesPath);
    strcat(databasePath, databaseNameGlobal);

    char* tablePath = malloc(strlen(databasePath) + strlen(tableName) + 5); // +5 for "/.txt" and null terminator
    strcpy(tablePath, databasePath);
    strcat(tablePath, "/");
    strcat(tablePath, tableName);
    strcat(tablePath, ".txt"); // Add the .txt extension

    // Open the file for reading
    FILE* file = fopen(tablePath, "r");
    if (file == NULL) {
        perror("Error opening file");
        return NULL;
    }

    char line[MAX_LINE_LENGTH];
    bool isDataSection = false;

    // Instead of printing, concatenate the data to a string
    char* result = malloc(MAX_LINE_LENGTH * sizeof(char));
    strcpy(result, ""); // Initialize an empty string

    while (fgets(line, MAX_LINE_LENGTH, file)) {
        if (strstr(line, ":") != NULL) {
            isDataSection = true; // Data section begins when a colon is found
        }
        if (isDataSection) {
            strcat(result, line); // Concatenate the data line
        }
    }

    fclose(file);

    return result;
}





int main() {
    int serverSocket, clientSocket;
    struct sockaddr_in serverAddr, clientAddr;
    socklen_t addrLen = sizeof(struct sockaddr_in);
    char buffer[MAX_BUFFER_SIZE];

    // Create socket
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    // Configure server address
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = INADDR_ANY;

    // Bind the socket
    if (bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        perror("Bind failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    // Listen for incoming connections
    if (listen(serverSocket, 5) == -1) {
        perror("Listen failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    printf("Server listening on port %d...\n", PORT);

    // Accept client connection
    if ((clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddr, &addrLen)) == -1) {
        perror("Accept failed");
        close(serverSocket);
        exit(EXIT_FAILURE);
    }

    printf("Client connected from %s:%d\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));


    // Receive and handle commands
    while (1) {
        memset(buffer, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, buffer, sizeof(buffer), 0) <= 0) {
            perror("Receive failed");
            close(serverSocket);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        handleCommand(buffer, clientSocket);

        // Check for EXIT command
        if (strncmp(buffer, "EXIT", 4) == 0) {
            printf("Server shutting down...\n");
            break;
        }
    }

    close(serverSocket);
    close(clientSocket);

    return 0;
}

void handleCommand(char* command, int clientSocket) {
    if (strncmp(command, "LOGIN", 5) == 0){
        char username[50];
        char password[50];
        if (sscanf(command, "LOGIN %s %s", username, password) == 2) {
            printf("Received username: %s\n", username);
            printf("Received password: %s\n", password);
            if (strcmp(username, "root")==0 && strcmp(password, "toor")==0){
                sudo = 1;
                printf("%d\n",sudo);
            }
            strcpy(usernameGlobal, username);
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else {
            send(clientSocket, "Invalid login format", sizeof("Invalid login format"), 0);
        }
    } else if (strncmp(command, "CREATE USER", 11) == 0){
        char nama_user[50];
        char password_user[50];
        sscanf(command, "CREATE USER %s IDENTIFIED BY %s;", nama_user, password_user);
        printf("nama_user: %s\n", nama_user);
        printf("password_user: %s\n", password_user);
        if (createUser(nama_user, password_user) == 1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (createUser(nama_user, password_user) == 0){
            send(clientSocket, "This function can only be executed with sudo privileges.", sizeof("This function can only be executed with sudo privileges."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else if (strncmp(command, "CREATE TABLE", 12) == 0) {
        char tableName[50];
        char columnsStr[500];
        sscanf(command, "CREATE TABLE %s (%[^);]", tableName, columnsStr);
        printf("Table Name: %s\n", tableName);
        printf("Columns: %s\n", columnsStr);

        // Create a copy of columnsStr that can be modified
        char columnsCopy[500];
        strcpy(columnsCopy, columnsStr);

        // Ubah ke array

        printf("Setelah inisiasi token\n");

        // Initialize an array to store the tokens
        char *columns[100];  // Assuming a maximum of 100 tokens, adjust as needed
        int count = 0;

        printf("Setelah inisiasi columns\n");

        char *token = strtok(columnsCopy, ",");
        // // Loop through the tokens and store them in the array
        while (token != NULL) {
            // Remove leading and trailing whitespaces from the token
            char *trimmedToken = trimSpaces(token);

            // Allocate memory for the token and copy it to the array
            columns[count] = malloc(strlen(trimmedToken) + 1);
            strcpy(columns[count], trimmedToken);

            // Move to the next token
            token = strtok(NULL, ",");
            ++count;
        }

        for (int i = 0; i < count; ++i) {
            printf("columns[%d] = %s\n", i, columns[i]);
        }

        printf("Setelah while\n");

        if (strlen(databaseNameGlobal)<=0){
            printf("Database belum dipilih");
            send(clientSocket, "Database not yet been chosen", sizeof("Database not yet been chosen"), 0);
        } else {
            if (createTable(databaseNameGlobal, tableName, columns, count) == 1) {
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else if (createTable(databaseNameGlobal, tableName, columns, count) == 0) {
                send(clientSocket, "Database doesn't exist.", sizeof("Database doesn't exist."), 0);
            } else {
                send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
            }
        }
    } else if (strncmp(command, "CREATE DATABASE", 15) == 0){
        char database[50];
        sscanf(command, "CREATE DATABASE %s", database);
        printf("database: %s\n", database);
        if (createDatabase(database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (createDatabase(database)==2){
            send(clientSocket, "database already exists.", sizeof("database already exists."), 0);
        } else if (createDatabase(database)==0){
            send(clientSocket, "Failed to create database.", sizeof("Failed to create database."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else if (strncmp(command, "USE", 3) == 0){
        char database[50];
        sscanf(command, "USE %s", database);
        printf("database: %s\n", database);
        if (useDatabase(database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            strcpy(databaseNameGlobal, database);
        } else if (useDatabase(database)==0){
            send(clientSocket, "database doesn't exist.", sizeof("database doesn't exist."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
        printf("%s\n", databasePathGlobal);
    } else if (strncmp(command, "GRANT PERMISSION", 16) == 0){
        char database[50];
        char nama_user[50];
        sscanf(command, "GRANT PERMISSION %s INTO %s", database, nama_user);
        printf("nama_user: %s\n", nama_user);
        printf("database: %s\n", database);
        if (grantPermissiontoaccessDatabase(nama_user, database)==1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (grantPermissiontoaccessDatabase(nama_user, database)==2){
            send(clientSocket, "database doesn't exist.", sizeof("database doesn't exist."), 0);
        } else if (grantPermissiontoaccessDatabase(nama_user, database)==0){
            send(clientSocket, "This function can only be executed with sudo privileges.", sizeof("This function can only be executed with sudo privileges."), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "DROP", 4) == 0){
        if (strncmp(command, "DROP DATABASE", 13) == 0){
            char database[50];
            sscanf(command, "DROP DATABASE %s", database);
            printf("database: %s\n", database);
            if (dropDatabase(database)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "Database Not Found", sizeof("Database Not Found"), 0);
            }
        } else if (strncmp(command, "DROP TABLE", 10) == 0){
            char table[50];
            sscanf(command, "DROP TABLE %s", table);
            printf("Table: %s\n", table);
            if (dropTable(databasePathGlobal, table)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "Table not found", sizeof("Table not found"), 0);
            }
        } else if (strncmp(command, "DROP COLUMN", 11) == 0){
            char kolom[50];
            char table[50];
            sscanf(command, "DROP COLUMN %s FROM %s", kolom, table);
            printf("Kolom: %s\n", kolom);
            printf("Table: %s\n", table);
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        }
    } else if (strncmp(command, "UPDATE", 6) == 0){
        char tableName[50];
        char columnName[50];
        char value[50];
        sscanf(command, "UPDATE %s SET %s=%[^;];", tableName, columnName, value);
        printf("Table Name: %s\n", tableName);
        printf("Column Name: %s\n", columnName);
        printf("Value: %s\n", value);
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "DELETE FROM", 11) == 0){
        char table[50];
        sscanf(command, "DELETE FROM %s", table);
        printf("Table: %s\n", table);
        if (strlen(databasePathGlobal)<=0){
            send(clientSocket, "Database not been chosen", sizeof("Database not been chosen"), 0);
        } else {
            if (clearTableData(table)){
                send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
            } else {
                send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
            }
        }
        send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
    } else if (strncmp(command, "INSERT INTO", 11) == 0){
        char table[50];
        char columns[500];
        sscanf(command, "INSERT INTO %s (%[^);]", table, columns);
        printf("Table: %s\n", table);
        printf("Isi: %s\n", columns);
        if (insertIntoTable(table, columns) == 1){
            send(clientSocket, "Command executed successfully", sizeof("Command executed successfully"), 0);
        } else if (insertIntoTable(table, columns) == 2){
            send(clientSocket, "Error data type", sizeof("Error data type"), 0);
        } else if (insertIntoTable(table, columns) == 3){
            send(clientSocket, "Columns not found", sizeof("Columns not found"), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }   
    } else if (strncmp(command, "SELECT FROM", 11)==0){
        char table[50];
        sscanf(command, "SELECT FROM %s", table);
        char* tableData = printTableData(table);
        printf("%s", tableData);
        if (strlen(tableData) > 0){
            send(clientSocket, tableData, strlen(tableData), 0);
            free(tableData);
        } else if (strlen(tableData) <= 0) {
            send(clientSocket, "There is a no data", sizeof("There is no data"), 0);
        } else {
            send(clientSocket, "There is a problem", sizeof("There is a problem"), 0);
        }
    } else {
        send(clientSocket, "Invalid command", sizeof("Invalid command"), 0);
    }
    logCommand(usernameGlobal, command);
}
