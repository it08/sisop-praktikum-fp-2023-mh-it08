#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 1000
#define MAX_STRING_SIZE 50

int extractUsername(const char *input, char *username) {
    const char *start = strchr(input, ':');
    if (start != NULL) {
        start++;  // Move to the character after the first ':'
        const char *end = strchr(start, ':');
        if (end != NULL) {
            // Calculate the length of the substring between the colons
            size_t length = end - start;

            // Ensure the length is within bounds
            if (length < MAX_STRING_SIZE - 1) {
                strncpy(username, start, length);
                username[length] = '\0';  // Null-terminate the string
                return 0;  // Success
            } else {
                return 1;  // Error: username is too long
            }
        }
    }

    return 1;  // Error: colons not found in the input
}


void process_log_line(char *line, const char *targetUsername) {
    // Find the position of the fourth colon (":") in the line
    char *pos = strchr(line, ':');
    char logUsername[MAX_STRING_SIZE];

    if (pos != NULL) {
        pos = strchr(pos + 1, ':');
        // printf("IF 1 = %s", pos);
        if (pos != NULL) {
            pos = strchr(pos + 1, ':');
            // printf("IF 2 = %s", pos);

            extractUsername(pos, logUsername);

            // printf("logUserName = %s\n", logUsername);

            if (strcmp(logUsername, targetUsername) == 0){
                if (pos != NULL) {
                    pos = strchr(pos + 1, ':');
                    // printf("IF 3 = %s", pos);
                    if (pos != NULL) {
                        // Extract the username from the log line

                        // printf("IF 4 = %s", pos);

                        // Trim leading whitespaces from the SQL query
                        pos += 1;
                        while (isspace(*pos)) {
                            pos++;
                        }

                        printf("%s", pos);
                    }
                }
            }
        }
    }
}

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s -u <username> -p <password>\n", programName);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    if (argc != 5 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
        printUsage(argv[0]);
    }

    char username[MAX_STRING_SIZE] = "";
    char password[MAX_STRING_SIZE] = "";

    strncpy(username, argv[2], MAX_STRING_SIZE - 1);
    strncpy(password, argv[4], MAX_STRING_SIZE - 1);

    FILE *file = fopen("log.txt", "r");
    if (file == NULL) {
        perror("Error opening file");
        return 1;
    }

    printf("Username: %s\n", username);
    printf("Password: %s\n", password);

    char line[MAX_LINE_LENGTH];

    printf("\n");

    // Read and process each line of the log file
    while (fgets(line, sizeof(line), file) != NULL) {
        process_log_line(line, username);
    }

    // Close the file
    fclose(file);

    return 0;
}
