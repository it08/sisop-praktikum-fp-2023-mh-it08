// client.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 3000
#define MAX_BUFFER_SIZE 1024
#define MAX_STRING_SIZE 50

void processCommandsFromFile(int clientSocket, const char *filename);

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s -u <username> -p <password>\n", programName);
    fprintf(stderr, "Usage: %s -u <username> -p <password> [-d <database>] [-f <filename>]\n", programName);
    exit(EXIT_FAILURE);
}

void handleSocketError(int clientSocket) {
    perror("Socket operation failed");
    close(clientSocket);
    exit(EXIT_FAILURE);
}

// Fungsi untuk autentikasi
int authenticate(const char *username, const char *password, const char *credentialFile) {
    printf("Jalan auten\n");

    FILE *credFile = fopen(credentialFile, "r");
    if (credFile == NULL) {
        perror("Error membuka file kredensial");
        return 0;  // Autentikasi gagal
    }

    char storedUsername[50];
    char storedPassword[50];
    int authenticated = 0;

    // Membaca setiap baris dalam file kredensial
    while (fscanf(credFile, "%s : %s", storedUsername, storedPassword) == 2) {
        // Memeriksa apakah username dan password cocok
        printf("storedUsername: %s\n", storedUsername);
        printf("storedPassword: %s\n", storedPassword);
        if (strcmp(username, storedUsername) == 0 && strcmp(password, storedPassword) == 0) {
            authenticated = 1;  // Autentikasi berhasil
            break;
        }
    }

    fclose(credFile);

    return authenticated;
}

int main(int argc, char *argv[]) {
    int clientSocket;
    struct sockaddr_in serverAddr;
    char command[MAX_BUFFER_SIZE];
    char response[MAX_BUFFER_SIZE];

    char username[MAX_STRING_SIZE] = "";
    char password[MAX_STRING_SIZE] = "";
    char database[MAX_STRING_SIZE] = "";

    const char *filename = NULL;

    if (geteuid() == 0) {
        printf("Running as root, performing actions without login.\n");
        strcpy(username, "root");
        strcpy(password, "toor");

        if (argc == 5 && strcmp(argv[1], "-d") == 0 && strcmp(argv[3], "-f") == 0) {
            strncpy(database, argv[2], MAX_STRING_SIZE - 1);
            filename = argv[4];
        } else if (argc != 1) {
            printUsage(argv[0]);
        }
    } else {
        if (argc < 4 || strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0) {
            printUsage(argv[0]);
            exit(EXIT_FAILURE);
        }

        if (authenticate(argv[2], argv[4], "users.txt")){
            strncpy(username, argv[2], MAX_STRING_SIZE - 1);
            strncpy(password, argv[4], MAX_STRING_SIZE - 1);
        } else {
            printf("Wrong Username and Password!");
            exit(EXIT_FAILURE);
        }

        if (argc == 9 && strcmp(argv[5], "-d") == 0 && strcmp(argv[7], "-f") == 0) {
            strncpy(database, argv[6], MAX_STRING_SIZE - 1);
            filename = argv[8];
        } else if (argc != 5) {
            printUsage(argv[0]);
        }
    }

    // Create socket
    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        handleSocketError(clientSocket);
    }

    // Configure server address
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(PORT);
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    // Connect to the server
    if (connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1) {
        handleSocketError(clientSocket);
    }

    printf("Connected to the server\n");

    //LOGIN

    snprintf(command, sizeof(command), "LOGIN %s %s", username, password);
    if (send(clientSocket, command, strlen(command), 0) == -1) {
        handleSocketError(clientSocket);
    }

    memset(response, 0, MAX_BUFFER_SIZE);
    if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
        handleSocketError(clientSocket);
    }

    printf("%s\n", response);

    //For database

    if (strlen(database) > 0) {
        snprintf(command, sizeof(command), "USE %s", database);
        if (send(clientSocket, command, strlen(command), 0) == -1) {
            handleSocketError(clientSocket);
        }
        memset(response, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
            handleSocketError(clientSocket);
        }

        printf("%s\n", response);
    }

    // Receive and handle commands
    if (filename != NULL) {
        processCommandsFromFile(clientSocket, filename);
        while (1) {
            printf("Enter command: ");
            fgets(command, MAX_BUFFER_SIZE, stdin);

            // Send the command to the server
            if (send(clientSocket, command, strlen(command), 0) == -1) {
                handleSocketError(clientSocket);
            }

            // Check for EXIT command
            if (strncmp(command, "EXIT", 4) == 0) {
                break;
            }

            // Receive and print the server's response
            memset(response, 0, MAX_BUFFER_SIZE);
            if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
                handleSocketError(clientSocket);
            }

            printf("%s\n", response);
        }
    } else {
        while (1) {
            printf("Enter command: ");
            fgets(command, MAX_BUFFER_SIZE, stdin);

            // Send the command to the server
            if (send(clientSocket, command, strlen(command), 0) == -1) {
                handleSocketError(clientSocket);
            }

            // Check for EXIT command
            if (strncmp(command, "EXIT", 4) == 0) {
                break;
            }

            // Receive and print the server's response
            memset(response, 0, MAX_BUFFER_SIZE);
            if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
                handleSocketError(clientSocket);
            }

            printf("%s\n", response);
        }
    }

    close(clientSocket);

    return 0;
}

void processCommandsFromFile(int clientSocket, const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    char command[MAX_BUFFER_SIZE];
    char response[MAX_BUFFER_SIZE];

    // Read and send commands from the file
    while (fgets(command, sizeof(command), file) != NULL) {
        // Remove newline characters
        command[strcspn(command, "\r\n")] = '\0';

        // Send the command to the server
        if (send(clientSocket, command, strlen(command), 0) == -1) {
            perror("Send failed");
            fclose(file);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        // Check for EXIT command
        if (strncmp(command, "EXIT", 4) == 0) {
            break;
        }

        // Receive and print the server's response
        memset(response, 0, MAX_BUFFER_SIZE);
        if (recv(clientSocket, response, sizeof(response), 0) <= 0) {
            perror("Receive failed");
            fclose(file);
            close(clientSocket);
            exit(EXIT_FAILURE);
        }

        printf("%s\n", response);
    }

    fclose(file);
}
